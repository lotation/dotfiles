```
L O F I   D O T F I L E S

```

![](Pictures/1.png)
![](Pictures/2.png)
![](Pictures/3.png)
<div style="text-align: center;">
	<a href="https://www.archlinux.org/">
		<img src="https://img.shields.io/badge/Arch-BTW-blue?logo=arch%20linux">
	</a> 
	<a href="https://www.gnu.org/licenses/gpl-3.0.en.html">
		<img src="https://img.shields.io/github/license/lotation/dotfiles">
	</a><br><br>
</div>

I'm currently daily driving Arch on my laptop with heavy customizations to extends the battery life as much as it's possible. It consumes ~10%/h with firefox (around 5 tabs open), an IDE like CLion or IntelliJ and some terminals.

I'm planning to write a script to easily switch from this dark theme to a light one (Solarized Light) since I mostly use my laptop in Uni at daytime.

Given that I no longer use this configuration so it might be a little outdated (kernel version in `neofetch`) and I don't guarantee everything works.


### My setup
* Editor: neovim, vscodium
* WM: i3 (gaps rounded corners)
* launcher: rofi
* Terminal: kitty
* Browser: chromium (now firefox)
* Shell: plain bash
* Colorscheme: pywal + some adjustments
* Music player: spotify + spicetify
* PDF viewer: zathura
* System monitor: gotop
* AUR helper: paru + custom script for clean build
* Fetch tools: neofetch, [`motd`](https://gitlab.com/lotation/motd "motd")
* Misc: redhift, feh, cava, dunst
